#!/bin/sh
#

# Initialize default environment
PID=$$
CWD=$(cd . && pwd)
PRG=$0
BASEDIR=$(cd "$(dirname ${PRG})" && pwd)
APPDIR=${BASEDIR}
BASENAME=$(basename "${PRG}")
APPNAME=${BASENAME}
while [ -L "${BASEDIR}/${BASENAME}" ]; do
    PRG=$(readlink "${BASEDIR}/$BASENAME")
    BASEDIR=$(cd "${BASEDIR}" && cd "$(dirname ${PRG})" && pwd)
    BASENAME=$(basename "${PRG}")
done

IS_DEBUG=$(${BASEDIR}/appdebug)
CFG_FILE="/root/.ssh/config"
HOST_FILE="/root/.ssh/known_hosts"

${BASEDIR}/sshgen 1> ${CFG_FILE}
if [ $? -ne 0 ]; then
    echo "Fatal error: generate config" >&2
    exit 1
fi

chmod 600 "$CFG_FILE"
if [ "$IS_DEBUG" == "YES" ]; then
    ${BASEDIR}/applector -v -nNT startup
else
    ${BASEDIR}/applector -nNT startup
fi
