package utils

import (
	"strconv"
	"strings"
)

// Trim unquote and trim string
func Trim(str string) string {
	res, err := strconv.Unquote(str)
	if err != nil {
		return strings.TrimSpace(str)
	}
	return strings.TrimSpace(res)
}

// TrimLower unquote, trim and convert to lowercase
func TrimLower(str string) string {
	res, err := strconv.Unquote(str)
	if err != nil {
		return strings.ToLower(strings.TrimSpace(str))
	}
	return strings.ToLower(strings.TrimSpace(res))
}
