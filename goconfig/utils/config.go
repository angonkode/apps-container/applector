package utils

import "os"

// IsDebug get debuggging status
func IsDebug() bool {
	val, keyExist := os.LookupEnv("APPDEBUG")
	if !keyExist {
		return false
	}

	switch TrimLower(val) {
	case "1", "t", "true", "yes":
		return true
	case "0", "f", "false", "no":
		return false
	}
	return false
}
