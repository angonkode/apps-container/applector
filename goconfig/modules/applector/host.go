package applector

import (
	"strconv"
	"strings"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/modules/log"
	"gitlab.com/angonkode/apps-container/applector.git/goconfig/utils"
)

// Host structure
type Host struct {
	Name     string
	User     string
	Hostname string
	Port     int
	Proxy    string
	Keys     []string
}

// HasProxy connect via proxy
func (host Host) HasProxy() bool {
	if len(host.Proxy) == 0 {
		return false
	}
	return true
}

// HasUser user not empty
func (host Host) HasUser() bool {
	if len(host.User) == 0 {
		return false
	}
	return true
}

// HasKeys has any ssh keys
func (host Host) HasKeys() bool {
	if len(host.Keys) == 0 {
		return false
	}
	return true
}

func newHost(name string, hostSpec string) Host {
	logger := log.Logger()

	var user string
	var host string
	var port int
	var proxy string
	var keys []string = []string{}

	ss := strings.Fields(utils.Trim(hostSpec))
	if len(ss) <= 0 {
		logger.Fatalln("Invalid host configuration:", name, ":", hostSpec)
	}

	for i, val := range ss {
		// processing connection part
		if i == 0 {
			user, host, port = parseHost(val)
			continue
		}

		mode, values := parseHostInfo(val)
		if mode == "proxy" {
			proxy = utils.Trim(values[0])
			continue
		}

		if mode == "key" {
			for _, v := range values {
				keys = append(keys, utils.Trim(v))
			}
			continue
		}
	}

	return Host{
		Name:     utils.Trim(name),
		User:     user,
		Hostname: host,
		Port:     port,
		Proxy:    proxy,
		Keys:     keys,
	}
}

func parseHost(str string) (user string, host string, port int) {
	logger := log.Logger()

	s := utils.Trim(str)
	ss := strings.Split(s, "@")
	var s1 string
	switch len(ss) {
	case 0:
		logger.Fatalln("Invalid host format:", str)
	case 1:
		user = ""
		s1 = utils.Trim(ss[0])
	default:
		user = utils.Trim(ss[0])
		s1 = utils.Trim(ss[1])
	}

	ss1 := strings.Split(s1, ":")
	switch len(ss1) {
	case 0:
		logger.Fatalln("Invalid host format:", str)
	case 1:
		host = utils.Trim(ss1[0])
		port = 22
	default:
		host = utils.Trim(ss1[0])
		if i, err := strconv.Atoi(utils.Trim(ss1[1])); err == nil {
			port = i
		} else {
			logger.WithError(err).Fatalln("Invalid port format:", ss1[1])
		}
	}
	return
}

func parseHostInfo(str string) (mode string, values []string) {
	ss := strings.Split(utils.Trim(str), ":")
	switch len(ss) {
	case 0:
		mode = ""
		values = []string{}
		return
	case 1:
		mode = strings.ToLower(utils.Trim(ss[0]))
		values = []string{}
		return
	default:
		mode = strings.ToLower(utils.Trim(ss[0]))
		values = strings.Split(ss[1], ",")
		return
	}
}
