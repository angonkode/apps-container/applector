package applector

import (
	"strconv"
	"strings"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/modules/log"
	"gitlab.com/angonkode/apps-container/applector.git/goconfig/utils"
)

// ServiceAddress service address
type ServiceAddress string

// Service applector service
type Service struct {
	Name        string
	Mode        string
	Source      ServiceAddress
	Destination ServiceAddress
}

func newService(name, serviceSpec string) Service {
	logger := log.Logger()
	ss := strings.Fields(utils.Trim(serviceSpec))
	if len(ss) >= 3 {
		return Service{
			Name:        utils.Trim(name),
			Mode:        utils.Trim(ss[0]),
			Source:      ServiceAddress(utils.Trim(ss[1])),
			Destination: ServiceAddress(utils.Trim(ss[2])),
		}
	}
	if len(ss) == 2 {
		return Service{
			Name:        utils.Trim(name),
			Mode:        utils.Trim(ss[0]),
			Source:      ServiceAddress(utils.Trim(ss[1])),
			Destination: ServiceAddress(""),
		}
	}
	logger.Fatalln("Invalid service configuration:", name, ":", serviceSpec)
	return Service{
		Name:        utils.Trim(name),
		Mode:        utils.Trim(ss[0]),
		Source:      ServiceAddress(""),
		Destination: ServiceAddress(""),
	}
}

// GetHost get host from tunnel address struct
func (svcAddr ServiceAddress) GetHost() string {
	addr := string(svcAddr)
	if len(addr) == 0 {
		return "0.0.0.0"
	}

	ss := strings.Split(addr, ":")
	if len(ss) < 2 {
		return "0.0.0.0"
	}

	hs := utils.Trim(ss[0])
	if len(hs) == 0 {
		return "0.0.0.0"
	}
	return hs
}

// GetPort get port number from tunnel address struct
func (svcAddr ServiceAddress) GetPort() int {
	addr := string(svcAddr)
	if len(addr) == 0 {
		return 0
	}

	ss := strings.Split(addr, ":")
	switch len(ss) {
	case 0:
		return 0
	case 1:
		i, err := strconv.Atoi(utils.Trim(ss[0]))
		if err != nil {
			return 0
		}
		return i
	default:
		i, err := strconv.Atoi(utils.Trim(ss[1]))
		if err != nil {
			return 0
		}
		return i
	}
}
