package applector

import (
	"os"
	"sort"
	"strconv"
)

var applector Host
var applectorServices map[string]Service = make(map[string]Service)
var applectorProxies map[string]Host = make(map[string]Host)
var applectorSSHKeys map[string]SSHKey = make(map[string]SSHKey)

// GetMaxService get maximum available service
func GetMaxService() int {
	val, keyExist := os.LookupEnv("MAX_SERVICE")
	if !keyExist {
		return 10
	}
	i, err := strconv.Atoi(val)
	if err == nil {
		return 10
	}
	return i
}

// GetApplector get applector host
func GetApplector() Host {
	return applector
}

// ListProxies list available proxies (sorted)
func ListProxies() []string {
	result := make([]string, len(applectorProxies))
	i := 0
	for key := range applectorProxies {
		result[i] = key
		i++
	}
	sort.Strings(result)
	return result
}

// ListServices list available services (sorted)
func ListServices() []string {
	result := make([]string, len(applectorServices))
	i := 0
	for key := range applectorServices {
		result[i] = key
		i++
	}
	sort.Strings(result)
	return result
}

// ListSSHKeys list available ssh keys (sorted)
func ListSSHKeys() []string {
	result := make([]string, len(applectorSSHKeys))
	i := 0
	for key := range applectorSSHKeys {
		result[i] = key
		i++
	}
	sort.Strings(result)
	return result
}

// GetProxyByName get proxy by name
func GetProxyByName(name string) *Host {
	proxy, exist := applectorProxies[name]
	if exist {
		return &proxy
	}
	return nil
}

// GetServiceByName get applector service by name
func GetServiceByName(name string) *Service {
	service, exist := applectorServices[name]
	if exist {
		return &service
	}
	return nil
}

// GetSSHKeyByName get ssh key by name
func GetSSHKeyByName(name string) *SSHKey {
	key, exist := applectorSSHKeys[name]
	if exist {
		return &key
	}
	return nil
}
