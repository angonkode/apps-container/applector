package main

import (
	"os"
	"testing"

	"gitlab.com/angonkode/apps-container/applector.git/goconfig/modules/applector"
)

func TestReader(t *testing.T) {
	// os.Setenv("APPLECTOR", "svc@target-host:22 proxy:PROXY1 key:KEY1,KEY3")
	os.Setenv("APPLECTOR", "\"gusto@172.26.11.14 proxy:PROXY1 key:KEY3\"")
	os.Setenv("PROXY1", "svcproxy@proxy1 proxy:PROXY2 key:KEY2")
	os.Setenv("PROXY2", "svcproxy@proxy2 key:KEY3")
	os.Setenv("SERVICE1", "LOCAL 6379 127.0.0.1:6379")
	os.Setenv("SERVICE2", "LOCAL 3306 127.0.0.1:3306")
	os.Setenv("SERVICE3", "REMOTE 8888 web:80")
	os.Setenv("KEY1", "/id_ed25519")
	os.Setenv("KEY2", "/id_ecdsa")
	os.Setenv("KEY3", "/id_rsa")

	defer os.Unsetenv("APPLECTOR")
	defer os.Unsetenv("PROXY1")
	defer os.Unsetenv("PROXY2")
	defer os.Unsetenv("SERVICE1")
	defer os.Unsetenv("SERVICE2")
	defer os.Unsetenv("SERVICE3")
	defer os.Unsetenv("KEY1")
	defer os.Unsetenv("KEY2")
	defer os.Unsetenv("KEY3")

	applector.LoadEnv()

	if err := sshConfigGen(); err != nil {
		t.Error(err)
	}
}
